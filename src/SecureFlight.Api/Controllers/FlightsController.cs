﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> _flightService;
    private readonly IService<PassengerFlight> _passengerFlightService;

    public FlightsController(IService<Flight> flightService, IMapper mapper, IService<PassengerFlight> passengerFlightService)
        : base(mapper)
    {
        _flightService = flightService;
        _passengerFlightService = passengerFlightService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }


    [HttpPost]
    public async Task<IActionResult>AddPassenger(long flightId, string passengerId)
    {
        var result = await _passengerFlightService.AddAsync(new PassengerFlight { FlightId = flightId, PassengerId = passengerId });
        return Ok(result);
    }
    
}